# license-approval-policies-test-cases

This projects contains test cases listed in https://gitlab.com/groups/gitlab-org/-/epics/8092#note_1278192815.

## Reports - how to test them

1. Clone this project to your group/namespace and ensure that in your group/namespace you have an Ultimate license.
1. For each policy file (`policy-1.yml`, `policy-2.yml`, `policy-3.yml`, `policy-4.yml`):
   1. Create new [Scan Result Policy](https://docs.gitlab.com/ee/user/application_security/policies/scan-result-policies.html) with YAML copied from `policy-N.yml` file:
      1. In your project go to `Security & Compliance` -> `Policies`.
      1. Click `New Policy` and select `Scan Result Policy`.
      1. Go to `.yaml mode` and paste the contents of the policy from the `policy-N.yml` file.
      1. Modify value of `group_approvers_ids` in `actions` array to ID of the Group that has access to your project (ie. [`11789105`](https://gitlab.com/groups/gitlab-org/govern/security-policies-backend))
      1. Click `Configure with merge request`.
      1. Merge newly created Merge Request.
1. For each `Test Case` listed in the table:
   1. On the default branch modify `.gitlab-ci.yml` to use the report from the `Default Branch Column`.
   1. Wait for the pipeline to complete.
   1. In `.gitlab-ci.yml` file modify `report-N.json` to use the report from `MR Branch Report` and create new MR with this change.
   1. Verify the behavior to match expected from `Approval Required` column.

| Test Case | Policy YAML    | Default Branch Report | MR Branch Report | Approval Required |
|-----------|----------------|-----------------------|------------------|-------------------|
| 1         | `policy-1.yml` | `report-0.json`       | `report-1.json`  | Yes               |
| 2         | `policy-1.yml` | `report-1.json`       | `report-2.json`  | No                |
| 3         | `policy-1.yml` | `report-2.json`       | `report-3.json`  | Yes               |
| 4         | `policy-1.yml` | `report-3.json`       | `report-4.json`  | No                |
| 5         | `policy-2.yml` | `report-0.json`       | `report-1.json`  | Yes               |
| 6         | `policy-2.yml` | `report-1.json`       | `report-2.json`  | No                |
| 7         | `policy-2.yml` | `report-2.json`       | `report-3.json`  | Yes               |
| 8         | `policy-2.yml` | `report-3.json`       | `report-4.json`  | Yes               |
| 9         | `policy-3.yml` | `report-0.json`       | `report-1.json`  | No                |
| 10        | `policy-3.yml` | `report-1.json`       | `report-2.json`  | Yes               |
| 11        | `policy-3.yml` | `report-2.json`       | `report-3.json`  | Yes               |
| 12        | `policy-3.yml` | `report-3.json`       | `report-4.json`  | Yes               |
| 13        | `policy-4.yml` | `report-0.json`       | `report-1.json`  | No                |
| 14        | `policy-4.yml` | `report-1.json`       | `report-2.json`  | Yes               |
| 15        | `policy-4.yml` | `report-2.json`       | `report-3.json`  | Yes               |
| 16        | `policy-4.yml` | `report-3.json`       | `report-4.json`  | Yes               |
